package com.springex.orm.bo;

import org.springex.orm.hibernate3.bean.Person;
import org.springex.orm.hibernate3.dao.PersonDao;



public class PersonBOImpl implements PersonBO {
	
	PersonDao personDao;
	

	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}

	public void save(Person person) {
		personDao.save(person);
	}

	public Person findPerson(int id) {
		return personDao.findPerson(id);
	}

	public void updatePerson(Person person) {
		personDao.updatePerson(person);
	}

	public void deletePerson(Person person) {
		personDao.deletePerson(person);
	}

}
