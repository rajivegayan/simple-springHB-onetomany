package com.springex.orm.bo;

import org.springex.orm.hibernate3.bean.Person;

/**
 * 
 * @author Lyanaya
 * This is Business Object Interface
 * Where the Business Logics goes here
 */

public interface PersonBO {
	
	void save(Person person);
	Person findPerson(int id);
	void updatePerson(Person person);
	void deletePerson(Person person);

}
