package org.springex.orm.hibernate3;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springex.orm.hibernate3.bean.Address;
import org.springex.orm.hibernate3.bean.Person;
import org.springex.orm.hibernate3.dao.PersonDao;
import org.springex.orm.hibernate3.dao.PersonDaoImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.springex.orm.bo.PersonBO;


public class App {

    public static void main( String[] args ) {
    
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
//    	PersonBO bo = (PersonBO)appContext.getBean("personBo1"); // Remove the Bean from Preson.xml to remove the BO
    	
    	PersonDao bo = (PersonDao)appContext.getBean("personDao");
 
    	// Creating a Person with addresses
    	/*
    	System.out.println("Starting to create Person --> Address");
    	
    	Address address1 = new Address();
    	address1.setId(1);
    	address1.setAddress("Unit 7 / 7 Clifton Street");
    	address1.setCity("Camden Park");
    	address1.setState("Adelaide");
    	address1.setZipPostal("SA 5038");
    	
    	Address address2 = new Address();
    	address2.setId(2);
    	address2.setAddress("No 18 Esela Sewana");
    	address2.setCity("Hirana");
    	address2.setState("Panadura");
    	address2.setZipPostal("11850");
    	
    	Address address3 = new Address();
    	address3.setId(3);
    	address3.setAddress("124/B Suriya Paluwa");
    	address3.setCity("Kadawatha");
    	address3.setState("Colombo");
    	address3.setZipPostal("11850");
    	
    	Set<Address> addressSet = new HashSet<Address>();
    	addressSet.add(address1);
    	addressSet.add(address2);
    	addressSet.add(address3);
    	
    	
    	
    	Person savePerson1 = new Person();
    	savePerson1.setId(1);
    	savePerson1.setFirstName("Rajive");
    	savePerson1.setLastName("Gayan");
    	savePerson1.setCreated(new Date());
    	savePerson1.setAddresses(addressSet);
    	bo.save(savePerson1);
    	System.out.println("Person and Addresses Successfully Created....");
    	
    	*/
    
    	//Retriving a Person
    	/*
    	Person retrivePer = bo.findPerson(1);
    	System.out.println("Found Peson id= "+retrivePer.getId());
    	System.out.println("Person Name = "+retrivePer.getFirstName());
    	System.out.println("Person Last Name = "+retrivePer.getLastName());
    	System.out.println("Person Created = "+retrivePer.getCreated());
    	*/
    	
    	// Deleting a Person by ID
    	/*
    	System.out.println("Deleting the people ...");
    	Person deletePerson =  new Person();
    	deletePerson.setId(1);
    	deletePerson.setFirstName("Rajive");
    	bo.deletePerson(deletePerson);
    	System.out.println("Completed the deleting...");
    	*/
    }
}
