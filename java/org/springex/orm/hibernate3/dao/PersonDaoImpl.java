package org.springex.orm.hibernate3.dao;

import java.util.List;

import org.springex.orm.hibernate3.bean.Person;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class PersonDaoImpl extends HibernateDaoSupport implements PersonDao {

	public void save(Person person) {
		getHibernateTemplate().save(person);
	}

	public Person findPerson(int id) {
		System.out.println("going to get the id owner = "+id);
//		List per = getHibernateTemplate().find("select p.PER_ID as PER_ID, p.FIRST_NAME as FIRST_NAME, p.LAST_NAME AS LAST_NAME from person p where p.PER_ID = ?",id);
		 List per = getHibernateTemplate().find("from Person p where p.id = ?",id);				 
		return (Person) per.get(0);
	}

	public void updatePerson(Person person) {
		getHibernateTemplate().update(person);
	}

	public void deletePerson(Person person) {
		getHibernateTemplate().delete(person);

	}

}
