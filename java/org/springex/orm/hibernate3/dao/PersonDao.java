package org.springex.orm.hibernate3.dao;

import org.springex.orm.hibernate3.bean.Person;

/**
 * 
 * @author Lyanaya
 * The Person Data Access Objects
 * All the CURD Operations are goes here
 */

public interface PersonDao {
	
	void save(Person person);
	Person findPerson(int id);
	void updatePerson(Person person);
	void deletePerson(Person person);
}
